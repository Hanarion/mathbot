from discord import Embed
from discord.ext import commands
import discord
from cogs.calcul import Calcul
from cogs.aide import CustomHelp
from cogs.matrice import Matrice

description = '''Un bot qui fait des maths.'''
bot = commands.Bot(command_prefix='=', description=description)


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')
    # channel = bot.get_channel(667053408636633088)
    # toto = await channel.fetch_message(722251413379612742)
    # await toto.delete()
    # toto = await channel.fetch_message(722251415514251326)
    # await toto.delete()


# @bot.event
# async def on_command_error(ctx: commands.Context, error):
#     command: commands.Command = ctx.command
#     embed = Embed(title="Erreur de syntaxe", description="**="+str(command)+"**")
#     embed.add_field(name='Alias', value=command.aliases, inline=False)
#     embed.add_field(name='Aide', value=command.help, inline=False)
#     await ctx.send(embed=embed)

bot.remove_command("help")

initial_extensions = ['cogs.aide', 'cogs.calcul', 'cogs.matrice', 'cogs.autre']

if __name__ == '__main__':
    for extension in initial_extensions:
        bot.load_extension(extension)

bot.run('')
