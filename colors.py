import json

colors = dict()

try:
    with open('json_couleur.json', 'r') as outfile:
        colors = dict(json.load(outfile))
except:
    pass