from sympy import *
from ast import literal_eval
import json


class MatriceExpr:
    matrice: Matrix

    def __init__(self, matrix: Matrix):
        self.matrice = matrix

    def save(self, user_id, matrice_save_name: str):
        matriceSave.save(self, user_id, matrice_save_name)
        matriceSave.saveall()

    def tostring(self):
        tosavelist: list = self.matrice.tolist()
        ressavelist = list()
        for x in tosavelist:
            tmpList = list()
            for y in x:
                if isinstance(y, Symbol) or isinstance(y, Expr):
                    tmpList.append(str(y))
                else:
                    tmpList.append(y)
            ressavelist.append(tmpList)
        return str(ressavelist)

    def determinant(self):
        return Determinant(self.matrice)

    def inverse(self):
        return Inverse(self.matrice)

    def transpose(self):
        return Transpose(self.matrice)

    def trace(self):
        return Trace(self.matrice)

    def add(self, toAdd):
        return MatAdd(self.matrice, toAdd)

    def mult(self, toMult):
        return MatMul(self.matrice, toMult)

    def sub(self, toSub):
        return MatAdd(self.matrice, -toSub)

    def pow(self, toPow):
        return MatPow(self.matrice, sympify(toPow))

    def test(self):
        return Trace(self.matrice)

    @staticmethod
    def matrixFromSize(x: int, y: int, val_defaut: int = 0):
        return MatriceExpr(Matrix([[val_defaut for xI in range(x)] for yI in range(y)]))

    @staticmethod
    def matrixFromText(text: str):
        matrice: list = literal_eval(text)
        matriceSympy = Matrix(matrice)
        return MatriceExpr(matriceSympy)

    @staticmethod
    def matrixFromStockage(user, name: str):
        # print(stockage_matrice)
        return matriceSave.get(user, name)

    @staticmethod
    def deleteMatrixFromStockage(user, name: str):
        matriceSave.remove(user, name)
        matriceSave.saveall()

    @staticmethod
    def matrixesFromStockage(user):
        return matriceSave.stockage_matrice[str(user)].keys()

    @staticmethod
    def matrixIdentite(n: int):
        return MatriceExpr(Identity(n))


class MatriceSave:
    stockage_matrice = dict()

    def __init__(self):
        try:
            with open('json_matrices.json') as json_file:
                tosave = json.load(json_file)
                for user in tosave.keys():
                    if user not in self.stockage_matrice.keys():
                        self.stockage_matrice[user] = dict()
                    for mat in tosave[user].keys():
                        self.stockage_matrice[user][mat] = MatriceExpr.matrixFromText(tosave[user][mat])
        except FileNotFoundError:
            pass

    def get(self, user, name):
        # print(self.stockage_matrice)
        return self.stockage_matrice[str(user)][name]

    def remove(self, user, name):
        self.stockage_matrice[str(user)].pop(name)

    def save(self, matrice, user_id, matrice_save_name):
        if str(user_id) not in self.stockage_matrice.keys():
            self.stockage_matrice[str(user_id)] = dict()
        # print(self.stockage_matrice)
        self.stockage_matrice[str(user_id)][matrice_save_name] = matrice

    def saveall(self):
        with open('json_matrices.json', 'w') as outfile:
            tosave = dict()
            for user in self.stockage_matrice.keys():
                tosave[user] = dict()
                for mat in self.stockage_matrice[user].keys():
                    tosave[user][mat] = self.stockage_matrice[str(user)][mat].tostring()
            # print(tosave)
            json.dump(tosave, outfile)


matriceSave = MatriceSave()
