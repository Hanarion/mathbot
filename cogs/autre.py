import json

from discord import File, Message
from discord.ext import commands
from sympy import SympifyError, Expr, sympify

from colors import colors
from latex import latexToImage


class Autre(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['tolatex', 'latex', 'tex'])
    async def mathlatex(self, ctx, *expression):
        """Transformer du texte en expression mathématique
        ⠀⠀⠀*Usage : =mathlatex <expression> [inline]*
        Ajouter \"inline\" à la fin permet de faire des affichages tels que \"f(x)=9\"
        """
        try:
            if expression[-1] == "inline":
                await ctx.send(file=File(latexToImage(ctx.author.id, ''.join(expression[:-1])), "resultat.png"))
            else:
                await ctx.send(file=File(latexToImage(ctx.author.id, sympify(''.join(expression))), "resultat.png"))
        except:
            await ctx.send(
                "Erreur de syntaxe, problèmes courants : 2x au lieu de 2\\*x, x² au lieu de x\\*\\*2, x^2 au lieu de x\\*\\*2\nAvez vous essayé en inline ? (voir =help mathlatex)")
        # except RuntimeError as e:
        #     print(str(e).replace('\\n', '\n'))
        message: Message = ctx.message
        try:
            await message.delete()
        except:
            pass

    @commands.command(aliases=['color', 'c', 'couleur'])
    async def setcolor(self, ctx):
        """Changer la couleur des images
        ⠀⠀⠀*Usage : =color*
        """
        with open('json_couleur.json', 'w') as outfile:
            if ctx.author.id in colors.keys():
                colors[ctx.author.id] = not colors[ctx.author.id]
            else:
                colors[ctx.author.id] = True

            json.dump(colors, outfile)
            if colors[ctx.author.id]:
                await ctx.send("Couleur de fond changée à **blanc**")
            else:
                await ctx.send("Couleur de fond changée à **noir**")

def setup(bot):
    bot.add_cog(Autre(bot))
