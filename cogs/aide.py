import discord
from discord.ext import commands


class CustomHelp(commands.Cog):
    """Liste toutes les catégories."""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, aliases=['aide', '?'])
    @commands.has_permissions(add_reactions=True, embed_links=True)
    async def help(self, ctx, *cog):
        """Liste toutes les catégories."""
        try:
            if not cog:
                # Cog listing.  What more?
                halp = discord.Embed(title='Liste des catégories de commande',
                                     description='Faire `=help *catégorie*` pour avoir plus d\'infos !')
                cogs_desc = ''
                for x in self.bot.cogs:
                    cogs_desc += ('{} - {}'.format(x, self.bot.cogs[x].__doc__) + '\n')
                halp.add_field(name='Catégories', value=cogs_desc[0:len(cogs_desc) - 1], inline=False)
                cmds_desc = ''
                for y in self.bot.walk_commands():
                    if not y.cog_name and not y.hidden:
                        cmds_desc += ('{} - {}'.format(y.name, y.help) + '\n')
                # halp.add_field(name='Uncatergorized Commands', value=cmds_desc[0:len(cmds_desc) - 1], inline=False)
                await ctx.message.add_reaction(emoji='✉')
                await ctx.message.author.send('', embed=halp)
            else:
                # Helps me remind you if you pass too many args.
                if len(cog) > 1:
                    halp = discord.Embed(title='Erreur!', description='Trop de catégories a la fois!',
                                         color=discord.Color.red())
                    await ctx.message.author.send('', embed=halp)
                else:
                    if cog[0][0].isupper():
                        # Command listing within a cog.
                        found = False
                        for x in self.bot.cogs:
                            for y in cog:
                                if x == y:
                                    halp = discord.Embed(title=cog[0] + ' Liste des commandes',
                                                         description=self.bot.cogs[cog[0]].__doc__)
                                    for c in self.bot.get_cog(y).get_commands():
                                        if not c.hidden:
                                            halp.add_field(name=c.name, value=c.help, inline=False)
                                    found = True
                        if not found:
                            # Reminds you if that cog doesn't exist.
                            halp = discord.Embed(title='Erreur!',
                                                 description='Cette catégorie est inconnue "' + cog[0] + '"?',
                                                 color=discord.Color.red())
                        else:
                            await ctx.message.add_reaction(emoji='✉')
                        await ctx.message.author.send('', embed=halp)
                    else:
                        # Command help
                        found = False
                        for command in self.bot.commands:
                            command: commands.Command
                            if cog[0] == command.name or cog[0] in command.aliases:
                                # print(command)
                                halp = discord.Embed(title='Aide de la commande ' + command.name,
                                                     description=command.help)
                                halp.add_field(name="Alias ", value=str(command.aliases), inline=False)
                                if isinstance(command, commands.Group):
                                    subcommandstext: str = ""
                                    subcommandstext2: str = ""

                                    command: commands.Group
                                    for subcommand in command.commands:
                                        subcommand: commands.Group
                                        if len(subcommandstext) < 900:
                                            subcommandstext += "**" + subcommand.name + "**" + " - " + subcommand.help + "\n"
                                        else:
                                            subcommandstext2 += "**" + subcommand.name + "**" + " - " + subcommand.help + "\n"
                                        # halp.add_field(name=subcommand.name, value=subcommand.help, inline=False)

                                    halp.add_field(name="Sous commandes ", value=subcommandstext, inline=False)
                                    if len(subcommandstext2) > 1:
                                        halp.add_field(name="Sous commandes (2)", value=subcommandstext2, inline=False)
                                # else:
                                #     # print(command.help)
                                #     halp.add_field(name="Usage ", value=str(command.usage))

                                found = True
                        if not found:
                            # Reminds you if that cog doesn't exist.
                            halp = discord.Embed(title='Erreur!',
                                                 description='Cette commande est inconnue "' + cog[0] + '"?',
                                                 color=discord.Color.red())
                        else:
                            await ctx.message.add_reaction(emoji='✉')
                        await ctx.message.author.send('', embed=halp)
        except TimeoutError as e:
            await ctx.send("Une erreur est survenue.")


def setup(bot):
    bot.add_cog(CustomHelp(bot))
