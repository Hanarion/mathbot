import discord
from discord.ext import commands
from sympy import NonSquareMatrixError, Eq

from cogs.matrices.matrice_expr import MatriceExpr
from latex import latexToImage


def is_number(string):
    try:
        float(string)
        return True
    except ValueError:
        return False


class Matrice(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group(aliases=["matrix", "mat"])
    async def matrice(self, ctx):
        """Permet de faire des opérations sur les matrices, ainsi que de les afficher
        Voir les sous commandes avec : **=help matrice**
        """
        if ctx.invoked_subcommand is None:
            if ctx.subcommand_passed is None:
                raise commands.MissingRequiredArgument(param="matrice")
            await ctx.send(
                'La sous-commande {0.subcommand_passed} n\'existe pas\n Faites =help matrice pour la liste des sous commandes'.format(
                    ctx))
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass

    @matrice.command(name='creer', aliases=['create', 'new'])
    async def _create(self, ctx, x: int, y: int, defautouname=None, name: str = None):
        """Permet de créer une matrice
        ⠀⠀⠀*Usage : =matrice creer <tailleX> <tailleY> [valeurParDéfaut] [nom]*"""
        if defautouname is not None and not is_number(defautouname):
            defaut = 0
            name = str(defautouname)
        else:
            defaut = defautouname
        if defautouname is None:
            defaut = 0
            name = None
        mat_expr = MatriceExpr.matrixFromSize(x, y, defaut)
        mat = mat_expr.matrice
        if name is not None:
            mat_expr.save(ctx.author.id, name)
            await ctx.send("Matrice sauvegardée en tant que " + name,
                           file=discord.File(latexToImage(ctx.author.id, mat), "resultat.png"))
        else:
            await ctx.send(file=discord.File(latexToImage(ctx.author.id, mat), "resultat.png"))
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass

    @matrice.command(name='identite', aliases=['ident', 'i'])
    async def _identite(self, ctx, n: int, name: str = None):
        """Permet de créer une matrice identité
        ⠀⠀⠀*Usage : =matrice creer <taille> [nom]*"""
        mat_expr = MatriceExpr.matrixFromSize(n, n)
        mat = mat_expr.matrice.pow(0).doit()
        if name is not None:
            MatriceExpr(mat_expr.pow(0).doit()).save(ctx.author.id, name)
            await ctx.send("Matrice sauvegardée en tant que " + name,
                           file=discord.File(latexToImage(ctx.author.id, mat), "resultat.png"))
        else:
            await ctx.send(file=discord.File(latexToImage(ctx.author.id, mat), "resultat.png"))
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass

    @matrice.command(name="import", aliases=['imp', 'importer'])
    async def _import(self, ctx: commands.Context, name: str, *text):
        """Permet d'importer une matrice depuis une liste de liste (style python)
        ⠀⠀⠀*Usage : =matrice import <nom> <text>*"""
        text = "".join(text)
        try:
            mat_expr = MatriceExpr.matrixFromText(text)
            mat = mat_expr.matrice

            mat_expr.save(ctx.author.id, name)
            await ctx.send("Matrice sauvegardée en tant que " + name,
                           file=discord.File(latexToImage(ctx.author.id, mat), "resultat.png"))
        except TimeoutError:
            await ctx.send(
                "Erreur, merci de préciser le nom avec lequel sauvegarder la matrice"
                "\n*Usage : =matrice import <nom> <text>*")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass

    @matrice.command(name="export", aliases=['exp', 'exporter'])
    async def _export(self, ctx: commands.Context, name: str):
        """Permet d'exporter une matrice depuis en une liste de liste (style python)
        ⠀⠀⠀*Usage : =matrice export <nom>*"""
        try:
            exported = MatriceExpr.matrixFromStockage(ctx.author.id, name).tostring()
            await ctx.send("Matrice exportée de " + name + "\n`" + exported + "`")
        except:
            await ctx.send(
                "Erreur, merci de préciser le nom avec lequel sauvegarder la matrice"
                "\n*Usage : =matrice import <nom> <text>*")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass
    @matrice.command(name="voir", aliases=['get', 'show', 'see', 'view'])
    async def _voir(self, ctx: commands.Context, name: str):
        """Permet de visualiser une matrice enregistrée
        ⠀⠀⠀*Usage : =matrice voir <nom>*"""
        try:
            mat_expr = MatriceExpr.matrixFromStockage(ctx.author.id, name)
            mat = mat_expr.matrice
            await ctx.send(file=discord.File(latexToImage(ctx.author.id, mat), "resultat.png"))
        except KeyError:
            await ctx.send(
                "Erreur, la matrice " + name + " n'a pas été trouvée")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass
    @matrice.command(name="determinant", aliases=['det', 'deter'])
    async def _determinant(self, ctx: commands.Context, name: str):
        """Donne le déterminant de la matrice enregistrée
        ⠀⠀⠀*Usage : =matrice det <nom>*"""
        try:
            mat_expr: MatriceExpr = MatriceExpr.matrixFromStockage(ctx.author.id, name)
            res = mat_expr.determinant()
            res = Eq(res, res.doit())
            await ctx.send(file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
        except KeyError:
            await ctx.send(
                "Erreur, la matrice " + name + " n'a pas été trouvée")
        except NonSquareMatrixError:
            await ctx.send(
                "Erreur, la matrice " + name + " n'est pas carrée (n(x)!=n(y)")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass
    @matrice.command(name="inverse", aliases=['inv', 'inverser'])
    async def _inverse(self, ctx: commands.Context, name: str, to_save: str = None):
        """Donne la matrice inverse de la matrice enregistrée
        ⠀⠀⠀*Usage : =matrice inv <nom> [nomDeSauvegarde]*"""
        try:
            mat_expr: MatriceExpr = MatriceExpr.matrixFromStockage(ctx.author.id, name)
            res1 = mat_expr.inverse()
            res = Eq(res1, res1.doit())
            if to_save is not None:
                MatriceExpr(res1.doit()).save(ctx.author.id, to_save)
                await ctx.send("Matrice sauvegardée en tant que " + to_save,
                               file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
            else:
                await ctx.send(file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
        except KeyError:
            await ctx.send(
                "Erreur, la matrice " + name + " n'a pas été trouvée")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass
    @matrice.command(name="transpose", aliases=["transp", "transposer"])
    async def _transpose(self, ctx: commands.Context, name: str, to_save: str = None):
        """Donne la matrice transposée de la matrice enregistrée
        ⠀⠀⠀*Usage : =matrice transpose <nom> [nomDeSauvegarde]*"""
        try:
            mat_expr: MatriceExpr = MatriceExpr.matrixFromStockage(ctx.author.id, name)
            res1 = mat_expr.transpose()
            res = Eq(res1, res1.doit())
            if to_save is not None:
                MatriceExpr(res1.doit()).save(ctx.author.id, to_save)
                await ctx.send("Matrice sauvegardée en tant que " + to_save,
                               file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
            else:
                await ctx.send(file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
        except KeyError:
            await ctx.send(
                "Erreur, la matrice " + name + " n'a pas été trouvée")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass
    @matrice.command(name="trace", aliases=["gettrace"])
    async def _trace(self, ctx: commands.Context, name: str, to_save: str = None):
        """Donne la trace de la matrice enregistrée
        ⠀⠀⠀*Usage : =matrice trace <nom> [nomDeSauvegarde]*"""
        try:
            mat_expr: MatriceExpr = MatriceExpr.matrixFromStockage(ctx.author.id, name)
            res1 = mat_expr.trace()
            res = Eq(res1, res1.doit())
            if to_save is not None:
                MatriceExpr(res1.doit()).save(ctx.author.id, to_save)
                await ctx.send("Matrice sauvegardée en tant que " + to_save,
                               file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
            else:
                await ctx.send(file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
        except KeyError:
            await ctx.send(
                "Erreur, la matrice " + name + " n'a pas été trouvée")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass
    @matrice.command(name="test")
    async def _test(self, ctx: commands.Context, name: str, to_save: str = None):
        """Commande de tests, ne pas utiliser
        ⠀⠀⠀*Usage : =matrice test <nom> [nomDeSauvegarde]*"""
        try:
            mat_expr: MatriceExpr = MatriceExpr.matrixFromStockage(ctx.author.id, name)
            res1 = mat_expr.test()
            res = Eq(res1, res1.doit())
            if to_save is not None:
                MatriceExpr(res1.doit()).save(ctx.author.id, to_save)
                await ctx.send("Matrice sauvegardée en tant que " + to_save,
                               file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
            else:
                await ctx.send(file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
        except KeyError:
            await ctx.send(
                "Erreur, la matrice " + name + " n'a pas été trouvée")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass
    @matrice.command(name="add", aliases=['ajouter', 'plus', 'ajout', '+'])
    async def _add(self, ctx: commands.Context, name1: str, name2: str, to_save: str = None):
        """Donne le résultat d'une addition de matrices enregistrées
        ⠀⠀⠀*Usage : =matrice add <nom1> <nom2>*"""
        try:
            mat_expr: MatriceExpr = MatriceExpr.matrixFromStockage(ctx.author.id, name1)
            mat_expr2: MatriceExpr = MatriceExpr.matrixFromStockage(ctx.author.id, name2)
            res1 = mat_expr.add(mat_expr2.matrice)
            res = Eq(res1, res1.doit())
            if to_save is not None:
                MatriceExpr(res1.doit()).save(ctx.author.id, to_save)
                await ctx.send("Matrice sauvegardée en tant que " + to_save,
                               file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
            else:
                await ctx.send(file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
        except KeyError as e:
            await ctx.send(
                "Erreur, la matrice " + str(e) + " n'a pas été trouvée")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass
    @matrice.command(name="sub", aliases=['soustraire', 'moins', '-'])
    async def _sub(self, ctx: commands.Context, name1: str, name2: str, to_save: str = None):
        """Donne le résultat d'une soustraction de matrices enregistrées
        ⠀⠀⠀*Usage : =matrice sub <nom1> <nom2>*"""
        try:
            mat_expr: MatriceExpr = MatriceExpr.matrixFromStockage(ctx.author.id, name1)
            mat_expr2: MatriceExpr = MatriceExpr.matrixFromStockage(ctx.author.id, name2)
            res1 = mat_expr.sub(mat_expr2.matrice)
            res = Eq(res1, res1.doit())
            if to_save is not None:
                MatriceExpr(res1.doit()).save(ctx.author.id, to_save)
                await ctx.send("Matrice sauvegardée en tant que " + to_save,
                               file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
            else:
                await ctx.send(file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
            await ctx.send("Note : Pour des raisons techniques, seul une addition est affichable")
        except KeyError as e:
            await ctx.send(
                "Erreur, la matrice " + str(e) + " n'a pas été trouvée")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass
    @matrice.command(name="mult", aliases=['multiplier', 'fois', 'multiplie', '*'])
    async def _mult(self, ctx: commands.Context, name1: str, name2: str, to_save: str = None):
        """Donne le résultat d'une multiplication de matrices enregistrées
        ⠀⠀⠀*Usage : =matrice mult <nom1> <nom2>*"""
        try:
            mat_expr: MatriceExpr = MatriceExpr.matrixFromStockage(ctx.author.id, name1)
            mat_expr2: MatriceExpr = MatriceExpr.matrixFromStockage(ctx.author.id, name2)
            res1 = mat_expr.mult(mat_expr2.matrice)
            res = Eq(res1, res1.doit())
            if to_save is not None:
                MatriceExpr(res1.doit()).save(ctx.author.id, to_save)
                await ctx.send("Matrice sauvegardée en tant que " + to_save,
                               file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
            else:
                await ctx.send(file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
        except KeyError as e:
            await ctx.send(
                "Erreur, la matrice " + str(e) + " n'a pas été trouvée")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass
    @matrice.command(name="pow", aliases=['puissance', '^', '**'])
    async def _pow(self, ctx: commands.Context, name1: str, name2: str, to_save: str = None):
        """Donne le résultat d'une puissance de matrices enregistrées
        ⠀⠀⠀*Usage : =matrice mult <nom1> <nom2>*"""
        try:
            mat_expr: MatriceExpr = MatriceExpr.matrixFromStockage(ctx.author.id, name1)
            res1 = mat_expr.pow(name2)
            res = Eq(res1, res1.doit())
            if to_save is not None:
                MatriceExpr(res1.doit()).save(ctx.author.id, to_save)
                await ctx.send("Matrice sauvegardée en tant que " + to_save,
                               file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
            else:
                await ctx.send(file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
        except KeyError as e:
            await ctx.send(
                "Erreur, la matrice " + str(e) + " n'a pas été trouvée")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass
    @matrice.command(name="delete", aliases=['del', 'rem', 'remove', 'suppr', 'supprimer'])
    async def _delete(self, ctx: commands.Context, name: str):
        """Permet de supprimer une matrice enregistrée
        ⠀⠀⠀*Usage : =matrice delete <nom>*"""
        try:
            MatriceExpr.deleteMatrixFromStockage(ctx.author.id, name)
            await ctx.send("Matrice " + name + " supprimée !")
        except KeyError:
            await ctx.send(
                "Erreur, la matrice " + name + " n'a pas été trouvée")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass
    @matrice.command(name="clear", aliases=['clr', 'removeall', 'delall'])
    async def _clear(self, ctx: commands.Context):
        """Permet de supprimer TOUTES les matrices enregistrées
        ⠀⠀⠀*Usage : =matrice clear*"""
        try:
            liste = list(MatriceExpr.matrixesFromStockage(ctx.author.id))
            for matrice in liste:
                MatriceExpr.deleteMatrixFromStockage(ctx.author.id, matrice)
            await ctx.send("Toutes les matrices ont étés supprimées")
        except KeyError:
            await ctx.send(
                "Une erreur est survenue")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass
    @matrice.command(name="list", aliases=['liste', 'l', 'ls'])
    async def _list(self, ctx: commands.Context):
        """Permet de lister les matrices enregistrée
        ⠀⠀⠀*Usage : =matrice list*"""
        try:
            liste = MatriceExpr.matrixesFromStockage(ctx.author.id)
            await ctx.send("Liste de mes matrices :\n" + str(list(liste)))
        except KeyError:
            await ctx.send(
                "Erreur, vous n'avez jamais créé de matrices !")
        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass

def setup(bot):
    bot.add_cog(Matrice(bot))
