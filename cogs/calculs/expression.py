from sympy import *


class Expression:
    expression: Expr
    source_expression: str

    def __init__(self, expr: str):
        self.source_expression = expr
        self.expression = sympify(expr)

    def evaluate(self):
        try:
            res = self.expression.evalf()
            if isinstance(res, Float):
                res = float(res)
                # if round(res) == res:
                #     res = round(res)
            return res
        except:
            return self.expression

    def simplify(self):
        return self.expression.simplify()

    def derivate(self):
        return Derivative(self.expression)

    def conjugate(self):
        return self.expression.conjugate()

    def integrate(self):
        return Integral(self.expression)

    def primitive(self):
        return self.expression.primitive()

    def limit(self, x, xlim):
        return Limit(self.expression, x, xlim)
