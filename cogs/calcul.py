import random

import discord
from discord.ext import commands
from sympy import SympifyError, oo, Eq, symbols, sympify
from sympy.plotting import plot

from cogs.calculs.expression import Expression
from latex import latexToImage
from os import remove as removefile


class Calcul(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["=", "calculer", "calc"])
    async def calcul(self, ctx: commands.Context, *expression: str):
        """Calcule une expression mathématique.
        *Usage : =calcul <expression>*"""
        try:
            expr: Expression = Expression(''.join(expression))
            # res = expr.evaluate()
            res = Eq(sympify(expr.source_expression, evaluate=False), expr.evaluate(), evaluate=False)
            embed = discord.Embed(title="Calcul de `" + str(expr.source_expression) + "`",
                                  description="Résultat : `" + str(expr.evaluate()) + "`")
            await ctx.send(embed=embed)
            await ctx.send(file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
        except SympifyError:
            await ctx.send(
                "Erreur de syntaxe, problèmes courants : 2x au lieu de 2\\*x, x² au lieu de x\\*\\*2, x^2 au lieu de x\\*\\*2")

        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass

    @commands.command(aliases=["simpl", "simplifie"])
    async def simplifier(self, ctx: commands.Context, *expression: str):
        """Simplifie une expression mathématique.
        *Usage : =simplifier <expression>*"""
        try:
            expr: Expression = Expression(''.join(expression))
            # res = expr.simplify()
            res = Eq(sympify(expr.source_expression, evaluate=False), expr.simplify(), evaluate=False)
            embed = discord.Embed(title="Simplification de `" + str(expr.source_expression) + "`",
                                  description="Résultat : `" + str(expr.simplify()) + "`")
            await ctx.send(embed=embed)
            await ctx.send(file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
        except SympifyError:
            await ctx.send(
                "Erreur de syntaxe, problèmes courants : 2x au lieu de 2\\*x, x² au lieu de x\\*\\*2, x^2 au lieu de x\\*\\*2")

        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass

    @commands.command(aliases=["derive", "derivation"])
    async def deriver(self, ctx: commands.Context, *expression: str):
        """Dérive une expression mathématique.
        *Usage : =deriver <expression>*"""
        try:
            expr: Expression = Expression(''.join(expression))
            res = expr.derivate().doit()
            # res2 = Eq(res, res.doit())
            embed = discord.Embed(title="Dérivée de `" + str(expr.source_expression) + "`",
                                  description="Résultat : `" + str(res) + "`")
            await ctx.send(embed=embed)
            await ctx.send(file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
        except SympifyError:
            await ctx.send(
                "Erreur de syntaxe, problèmes courants : 2x au lieu de 2\\*x, x² au lieu de x\\*\\*2, x^2 au lieu de x\\*\\*2")

        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass

    @commands.command(aliases=["conj", "conjugée"])
    async def conjuguer(self, ctx: commands.Context, *expression: str):
        """Renvoie la conjuguée d'une expression mathématique.
        *Usage : =conjuguer <expression>*"""
        try:
            expr: Expression = Expression(''.join(expression))
            res = expr.conjugate()
            embed = discord.Embed(title="Conjugée de `" + str(expr.source_expression) + "`",
                                  description="Résultat : `" + str(res) + "`")
            await ctx.send(embed=embed)
            await ctx.send(file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
        except SympifyError:
            await ctx.send(
                "Erreur de syntaxe, problèmes courants : 2x au lieu de 2\\*x, x² au lieu de x\\*\\*2, x^2 au lieu de x\\*\\*2")

        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass

    @commands.command(aliases=["intégrale", "integ"])
    async def integrale(self, ctx: commands.Context, *expression: str):
        """Retourne l'intégrale d'une expression mathématique.
        *Usage : =integrale <expression>*"""
        try:
            expr: Expression = Expression(''.join(expression))
            res = expr.integrate()
            res2 = Eq(res, res.doit())
            embed = discord.Embed(title="Intégrale de `" + str(expr.source_expression) + "`",
                                  description="Résultat : `" + str(res) + "`")
            await ctx.send(embed=embed)
            await ctx.send(file=discord.File(latexToImage(ctx.author.id, res2), "resultat.png"))
        except SympifyError:
            await ctx.send(
                "Erreur de syntaxe, problèmes courants : 2x au lieu de 2\\*x, x² au lieu de x\\*\\*2, x^2 au lieu de x\\*\\*2")

        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass

    @commands.command(aliases=["primit", "prim"])
    async def primitive(self, ctx: commands.Context, *expression: str):
        """Retourne la primitive d'une expression mathématique.
        *Usage : =primitive <expression>*"""
        try:
            expr: Expression = Expression(''.join(expression))
            res = expr.primitive()
            embed = discord.Embed(title="Primitive de `" + str(expr.source_expression) + "`",
                                  description="Résultat : `" + str(res) + "`")
            await ctx.send(embed=embed)
            await ctx.send(file=discord.File(latexToImage(ctx.author.id, res), "resultat.png"))
        except SympifyError:
            await ctx.send(
                "Erreur de syntaxe, problèmes courants : 2x au lieu de 2\\*x, x² au lieu de x\\*\\*2, x^2 au lieu de x\\*\\*2")

        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass

    @commands.command(aliases=["lim", "limit"])
    async def limite(self, ctx: commands.Context, *expression: str):
        """Retourne les limites d'une expression mathématique.
        *Usage : =limite \"<expression>\" <tend_vers>*"""
        try:
            expr: Expression = Expression(''.join(expression[:-1]))
            if expression[-1].lower().startswith("+inf") or expression[-1].lower() == "+oo" or expression[
                -1].lower() == "oo":
                tend_vers = oo
            elif expression[-1].lower().startswith("-inf") or expression[-1].lower() == "-oo":
                tend_vers = -oo
            else:
                tend_vers = expression[-1]
            res = expr.limit(list(expr.expression.free_symbols)[0], tend_vers)
            res2 = Eq(res, res.doit())
            embed = discord.Embed(title="Limites de `" + str(expr.source_expression) + "` pour x => " + str(tend_vers))
            await ctx.send(embed=embed)
            await ctx.send(file=discord.File(latexToImage(ctx.author.id, res2), "resultat.png"))
        except SympifyError:
            await ctx.send(
                "Erreur de syntaxe, problèmes courants : 2x au lieu de 2\\*x, x² au lieu de x\\*\\*2, x^2 au lieu de x\\*\\*2" +
                "\n*Usage : =limite \"<expression>\" <tend_vers>*")

        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass

    @commands.command(aliases=["g", "plot", "grapher"])
    async def graphe(self, ctx: commands.Context, *expression: str):
        """Retourne un graphe d'une expression mathématique.
        *Usage : =graphe \"<expression>\"*"""
        try:
            expr = ''.join(expression).replace('abs(', 'Abs(')
            p = plot(sympify(expr), show=False, backend='matplotlib', adaptative=True)
            p.save("tmp/" + str(ctx.message.id) + ".png")
            with open("tmp/" + str(ctx.message.id) + ".png", 'rb') as img:
                embed = discord.Embed(title="Graphe de `" + str(expr) + "` :")
                await ctx.send(embed=embed)
                await ctx.send(file=discord.File(img, "resultat.png"))
                removefile("tmp/" + str(ctx.message.id) + ".png")
        except SympifyError:
            await ctx.send(
                "Erreur de syntaxe, problèmes courants : 2x au lieu de 2\\*x, x² au lieu de x\\*\\*2, x^2 au lieu de x\\*\\*2" +
                "\n*Usage : =limite \"<expression>\" <tend_vers>*")

        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass
    @commands.command(aliases=["plotlimit", "grapherlimite", "gl", 'graphlim', 'pl'])
    async def graphelimite(self, ctx: commands.Context, min: int, max: int, *expression: str):
        """Retourne un graphe d'une expression mathématique avec limites personnalisées.
        *Usage : =graphe <min> <max> \"<expression>\"*"""
        try:
            expr = ''.join(expression).replace('abs(', 'Abs(')
            p = plot(expr, (symbols('x'), int(min), int(max)), show=False, backend='matplotlib', adaptative=True)
            p.save("tmp/" + str(ctx.message.id) + ".png")
            with open("tmp/" + str(ctx.message.id) + ".png", 'rb') as img:
                embed = discord.Embed(title="Graphe de `" + str(expr) + "` :")
                await ctx.send(embed=embed)
                await ctx.send(file=discord.File(img, "resultat.png"))
                removefile("tmp/" + str(ctx.message.id) + ".png")
        except SympifyError:
            await ctx.send(
                "Erreur de syntaxe, problèmes courants : 2x au lieu de 2\\*x, x² au lieu de x\\*\\*2, x^2 au lieu de x\\*\\*2" +
                "\n*Usage : =limite \"<expression>\" <tend_vers>*")

        message: discord.Message = ctx.message
        try:
            await message.delete()
        except:
            pass

def setup(bot):
    bot.add_cog(Calcul(bot))
