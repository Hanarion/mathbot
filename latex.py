from io import BytesIO

from PIL import Image, ImageOps
from sympy import preview
from cairosvg import svg2png

from colors import colors


def latexToImage(id, latex_compatible):
    if id in colors.keys():
        white = colors[id]
    else:
        white = False
    # print(latex_compatible)
    svg = BytesIO()
    preview(latex_compatible, output='svg', viewer='BytesIO', outputbuffer=svg, packages=('amsmath',))
    svg.seek(0)
    svg_str = svg.read().decode("UTF-8")
    # print(svg_str)
    png = BytesIO()
    if not white:
        svg_str = svg_str.replace('<path ', '<path fill="white" ').replace('<rect ',
                                                                           '<rect fill="white" ')
    # else:
    # svg_str = svg_str.replace('<g ', '<g style="background-color:white" ')
    # print(svg_str)
    svg2png(bytestring=svg_str.encode(
        'utf-8'),
        write_to=png, scale=2)
    png.seek(0)
    res = BytesIO()
    if white:
        img_pil = Image.open(png)
        img_pil.load()
        background = Image.new("RGB", img_pil.size, (255, 255, 255))
        background.paste(img_pil, mask=img_pil.split()[3])
        background = ImageOps.expand(background, border=10, fill="White")
        background.save(res, "JPEG", quality=100)
        #
        # old_im = Image.open(res)
        # old_size = old_im.size
        #
        # new_size = (old_size[0] + 20, old_size[1] + 20)
        # new_im = Image.new("RGB", new_size, 255)  ## luckily, this is already black!
        # new_im.paste(old_im, ((new_size[0] - old_size[0]) / 2,
        #                       (new_size[1] - old_size[1]) / 2))
        # res = new_im
    else:
        res = png
    res.seek(0)

    return res
